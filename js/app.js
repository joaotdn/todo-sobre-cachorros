// Foundation JavaScript
// Documentation can be found at: http://foundation.zurb.com/docs
$(document).foundation();

/*
	Show Formulario de busca mobile
 */
$('.icon-search').on('touchstart click', function(event) {
	event.preventDefault();

	if(!$(this).hasClass('mf')) {
		if(!$('.mobile-search').hasClass('show')) {
			$('.mobile-search').addClass('show').find('input[type="text"]').focus();
		} else {
			$('.mobile-search').removeClass('show');
		}	
	} else {
		if(!$('form.mf').hasClass('show')) {
			$('form.mf').addClass('show').find('input[type="text"]').focus();
		} else {
			$('form.mf').removeClass('show');
		}
	}
	
});

/*
	Clonar conteudo de menu desktop para menu mobile
 */
$('li','.menu-top').clone().appendTo('.off-canvas-list');
$('li','.main-items').clone().appendTo('#sub-fixed');
$('li','.main-items').clone().appendTo('.off-canvas-list');
$('div','.ade-slide').clone().appendTo('.ade-mobile');
$('.newsletter').clone().appendTo('.newsletter-mobile');
//$('li','.social-links').clone().appendTo('.off-canvas-list');

$('div','.foods-desk').clone().appendTo('.foods-mobile ul');

$('div','.foods-mobile').each(function(index, el) {
	$(this).wrap('<li></li>')	
});

/*
	Atributo data-thumb como background do seletor
 */
$('*[data-thumb]').each(function() {
	$(this).css({
		backgroundImage: 'url('+ $(this).data('thumb') +')'
	});
});

/*
	data-thumb-mo para sobstituir miniaturas em tablets e smartphones
 */
$('*[data-thumb-mo]','.mobile-thumb').each(function() {
	$(this).find('img').attr('src',$(this).data('thumb-mo'));
});

$(document).ready(function() {
	$(".list-slide").zAccordion({ 
		//easing: 'easeOutBounce',
		timeout: 8500,
		speed: 500,
		slideWidth: 600,
		width: 766,
		height: 280,
		trigger: 'mouseover',
		startingSlide: 0,
		slideClass: 'slide',
		animationStart: function () {
			$('.list-slide').find('li.slide-open .icon-arrow').fadeOut();
			$('.list-slide').find('li.slide-previous h3').fadeOut();
		},
		animationComplete: function () {
			$('.list-slide').find('li.slide-previous .icon-arrow').fadeIn();
			$('.list-slide').find('li.slide-open h3').fadeIn();
			$('.list-slide').find('li.slide-closed .icon-arrow').fadeIn();
		},
		buildComplete: function () {
			$('.list-slide').find('li.slide-previous .icon-arrow').fadeIn();
			$('.list-slide').find('li.slide-closed h3').css('display', 'none');
			$('.list-slide').find('li.slide-open .icon-arrow').fadeOut();
		}
	});
});

function updateArrowsSliderAfter(parent, next, prev) {
	var pagerFirst = $('.owl-page:first',parent),
		pagerLast = $('.owl-page:last',parent),
		iconLeft = $(next),
		iconRight = $(prev),
		navSliderPos = $('.owl-wrapper',parent);

	if(pagerLast.hasClass('active')) {
		iconLeft.addClass('less-opacity');
	} else {
		iconLeft.removeClass('less-opacity');
	}
};

/*
	Carrossel para blocos de conteudo para Psicologia
 */
(function(){
	var contentPsi = $("#content-carousel-psi");

	contentPsi.owlCarousel({
		responsiveBaseWidth: $('#content-carousel-psi'),
		pagination: true,
		itemsCustom : [
	        [200, 2],
	        [419, 2],
	        [700, 3],
	        [766, 3],
	      ],
	    //rewindNav: false
	});

	$(".next-psi").click(function(e){
		e.preventDefault();
	    contentPsi.trigger('owl.next');
	});
	$(".prev-psi").click(function(e){
		e.preventDefault();
	  contentPsi.trigger('owl.prev');
	});
}());

/*
	Carrossel para blocos de conteudo para Adestramento
 */
(function(){
	$('div','.ade-mobile').removeClass('small-7 medium-7 large-14 left');

	var contentAde = $("#content-carousel-ade");

	contentAde.owlCarousel({
		responsiveBaseWidth: $('#content-carousel-ade'),
		pagination: true,
		itemsCustom : [
	        [200, 2],
	        [419, 2],
	        [766, 3],
	      ],
	});

	$(".next-ade").click(function(e){
		e.preventDefault();
	    contentAde.trigger('owl.next');
	});
	$(".prev-ade").click(function(e){
		e.preventDefault();
	  contentAde.trigger('owl.prev');
	});
}());

/*
	Carrossel para blocos de conteudo para Videos
 */
(function(){
	var contentVid = $("#content-carousel-vid");

	contentVid.owlCarousel({
		responsiveBaseWidth: $('#content-carousel-vid'),
		pagination: true,
		itemsCustom : [
	        [200, 2],
	        [748, 3],
	        [1080, 5],
	      ],
	});

	$(".next-vid").click(function(e){
		e.preventDefault();
	    contentVid.trigger('owl.next');
	});
	$(".prev-vid").click(function(e){
		e.preventDefault();
	  contentVid.trigger('owl.prev');
	});
}());


/*
	Carrossel para guia de raças
 */
(function(){
	var contentVid = $(".list-dogs");

	contentVid.owlCarousel({
		responsiveBaseWidth: $('.list-dogs'),
		pagination: false,
		itemsCustom : [
	        [829,10],
	        [800,10],
	        [728,6],
	        [560,5],
	        [460,5],
	        [200,3]
	      ],
	});

	$(".next-dog").click(function(e){
		e.preventDefault();
	    contentVid.trigger('owl.next');
	});
	$(".prev-dog").click(function(e){
		e.preventDefault();
	  contentVid.trigger('owl.prev');
	});
}());

/*
	Carrossel para produtos
 */
(function(){
	var contentPsi = $(".list-products");

	contentPsi.owlCarousel({
		responsiveBaseWidth: $('.list-products'),
		pagination: false,
		itemsCustom : [
	        [488, 3],
	        [200, 2],
	      ],
	});

	$(".next-pro").click(function(e){
		e.preventDefault();
	    contentPsi.trigger('owl.next');
	});
	$(".prev-pro").click(function(e){
		e.preventDefault();
	  contentPsi.trigger('owl.prev');
	});
}());

//ToolTips plugin
$.fn.tooltipster('setDefaults', {
  position: 'bottom'
});

$('.tooltip-dogs').tooltipster({
    contentAsHTML: true,
    positionTracker: true,
    position: 'bottom',
    animation: 'grow'
});

//Show menu fixo
$(document).on('scroll', window, function(event) {
	//event.preventDefault();
	/* Act on the event */
	var topHeader = $('body').scrollTop();

	if(topHeader >= 250) {
		$('.fixed-menu').addClass('active');
	} else {
		$('.fixed-menu').removeClass('active');
	}

});
