<?php
//get all post IDs for posts beginning with cap B, in title order,
//display posts
 
$first_char = 'B';
 
$postids=$wpdb->get_col($wpdb->prepare("
SELECT      ID
FROM        $wpdb->posts
WHERE       SUBSTR($wpdb->posts.post_title,1,1) = %s
ORDER BY    $wpdb->posts.post_title",$first_char));
 
if ($postids) {
$args=array(
  'post__in' => $postids,
  'post_type' => 'post',
  'post_status' => 'publish',
  'cat'=>2, //List posts only from category 2
  'posts_per_page' => -1,
  'caller_get_posts'=> 1
);
$my_query = null;
$my_query = new WP_Query($args);
if( $my_query->have_posts() ) {
 echo 'Posts that begin with '. $first_char;
 
  while ($my_query->have_posts()): $my_query->the_post(); ?>
 
    <p><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></p>
    <?php
       
  endwhile;
}
wp_reset_query();  // Restore global post data stomped by the_post().
}
?>